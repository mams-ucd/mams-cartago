package mams.hal.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.hal.Constants;
import mams.hal.artifacts.TypedResourceArtifact;
import mams.handlers.ResourceHandler;
import mams.web.Handler;
import mams.web.Path;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class JavaTypesHandler extends ResourceHandler {
    public JavaTypesHandler(TypedResourceArtifact artifact) {
        super(artifact);
    }

	@SuppressWarnings("unchecked")
    public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        System.out.println("Path: "  +path);
        this.path = path;
        if (this.path.length() > 0) {
            String prefix = this.path.prefix();
            if (prefix.equals(Constants.PROFILE)) {
                System.out.println("SPECIAL CASE");
                Class<TypedResourceArtifact> cls = (Class<TypedResourceArtifact>) Class.forName(((TypedResourceArtifact) artifact).getClassName());
                // Create the json schema here...
                WebServer.writeResponse(ctx, request,
                    ResponseEntity
                        .type("application/json")
                        .status(HttpResponseStatus.OK)
                        .body("HELLO: " +cls.getCanonicalName())
                );//.body(mapper.writeValueAsString(getObject()));
                
                // This would be a hook to display the json schema for the resource
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.BAD_GATEWAY);
                return;
            }

            Handler handler = routes.get(prefix);
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, this.path);
            }
        } else {
            // System.out.println("Method: " + request.method().toString());
            // System.out.println("Uri: " + request.uri().toString());
            artifact.handle(ctx, request);
        }
	}
}

