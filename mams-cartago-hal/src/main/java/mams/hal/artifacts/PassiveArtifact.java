package mams.hal.artifacts;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.utils.HttpContext;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class PassiveArtifact extends TypedResourceArtifact {
    protected Map<HttpMethod, Function<HttpContext, ResponseEntity>> methods = new HashMap<HttpMethod,Function<HttpContext, ResponseEntity>>();

    {
        set(HttpMethod.OPTIONS, context -> {
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (HttpMethod method : methods.keySet()) {
                if (first) first=false; else builder.append(",");
                builder.append(method.asciiName());
            }
            return new ResponseEntity(HttpResponseStatus.NO_CONTENT).header("Allow", builder.toString());
        });
        set(HttpMethod.GET, context -> {
            try {
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getObject()));
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }

    protected void set(HttpMethod method, Function<HttpContext,ResponseEntity> context) {
        methods.put(method, context);
    }

    public boolean handle(ChannelHandlerContext ctx, FullHttpRequest request) {
        Function<HttpContext, ResponseEntity> method = methods.get(request.method());

        if (method == null) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            WebServer.writeResponse(ctx, request, method.apply(new HttpContext(request, ctx)));
        }
        return true;
    }
}
