package mams.hal.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import mams.artifacts.BaseArtifact;
import mams.hal.handlers.BaseJavaTypesHandler;
import mams.handlers.BaseHandler;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class BaseJavaTypesArtifact extends BaseArtifact {
	protected BaseHandler createBaseHandler(String context) {
		return new BaseJavaTypesHandler(this,context);
	}


}
