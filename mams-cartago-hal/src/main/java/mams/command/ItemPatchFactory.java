package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.TypedResourceArtifact;
import mams.active.HTTPRequest;

public class ItemPatchFactory implements CommandFactory {

    @Override
    public Command create(TypedResourceArtifact artifact, HTTPRequest event) {
        return new ItemPatchCommand(artifact, event.getCtx(), event.getRequest(), event.getType());
    }

    @Override
    public Command create(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request) {
        return new ItemPatchCommand(artifact, ctx, request, artifact.getType());
    }


}