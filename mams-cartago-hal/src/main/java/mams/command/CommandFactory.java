package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.active.HTTPRequest;
import mams.artifacts.TypedResourceArtifact;

public interface CommandFactory {
    Command create(TypedResourceArtifact artifact, HTTPRequest event);
    Command create(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request);
}