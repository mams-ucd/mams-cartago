package mams.command;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.AbstractItemArtifact;
import mams.artifacts.ResourceArtifact;
import mams.artifacts.TypedResourceArtifact;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class PassiveListGetCommand implements Command {
    private static ObjectMapper mapper = new ObjectMapper();

    private TypedResourceArtifact artifact;
    private ChannelHandlerContext ctx;
    private FullHttpRequest request;
    private String type;

    public PassiveListGetCommand(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request,String type) {
        this.artifact = artifact;
        this.ctx = ctx;
        this.request = request;
        this.type = type;
	}

    public boolean execute() {
        try{
            ArrayNode arrayNode = mapper.createArrayNode();
            for (ResourceArtifact childArtifact : artifact.getChildren()) {
                arrayNode.add(((AbstractItemArtifact) childArtifact).createItemNode(new ResourceArtifact[]{}));
            }

            ObjectNode typeNode = mapper.createObjectNode();
            typeNode.set(type, arrayNode);

            ObjectNode selfNode = mapper.createObjectNode();
            selfNode.put("href", artifact.getUri());
            ObjectNode links = mapper.createObjectNode();
            links.set("self", selfNode);

            ObjectNode node = mapper.createObjectNode();
            node.set("_embedded", typeNode);
            node.set("_links", links);
            node.put("_total", arrayNode.size());

            String json = mapper.writeValueAsString(node);
            
            WebServer.writeResponse(ctx, request, ResponseEntity.type("application/hal+json").status(HttpResponseStatus.OK).body(json));
        }catch( Exception e){
           e.printStackTrace();
           WebServer.writeResponse(ctx, request, ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage()));
           return false;
        }
        return true;
    }


}
