package mams.command;

import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class ItemPutCommand extends AbstractItemCommand {
    public ItemPutCommand(ResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request, String type) {
        super(artifact, ctx, request, type);
    }

    public boolean execute() {
        try {
            CartagoBackend
                .getInstance()
                .doAction(
                    artifact.getId(), 
                    new Op("updateResource", request.headers().get("Content-Type"), Utils.getBody(request)));
            
            WebServer.writeResponse(ctx, request, ResponseEntity.type("application/json").status(HttpResponseStatus.OK));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}