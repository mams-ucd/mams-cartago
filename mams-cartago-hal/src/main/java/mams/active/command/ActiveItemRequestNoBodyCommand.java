package mams.active.command;

import cartago.CartagoException;
import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.TypedResourceArtifact;
import mams.command.AbstractRequestCommand;
import mams.utils.CartagoBackend;
import mams.utils.HTTPRequest;

public class ActiveItemRequestNoBodyCommand extends AbstractRequestCommand {
    public ActiveItemRequestNoBodyCommand(TypedResourceArtifact artifact, ChannelHandlerContext ctx,
            FullHttpRequest request) {
        super(artifact, ctx, request);
    }

    public boolean execute() {
        HTTPRequest httpRequest = new HTTPRequest(CACHE_ID++, getMethod(), artifact.getType(), ctx, request);
        // httpRequest.setData(artifact.getObject());

        try {
            CartagoBackend.getInstance().doAction(artifact.getId(), new Op("enqueue", httpRequest));
        } catch (CartagoException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
}