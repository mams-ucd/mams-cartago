package mams.messaging;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.fasterxml.jackson.databind.JsonNode;

import astra.core.Agent;
import astra.formula.Formula;
import astra.messaging.AstraMessage;
import mams.adaptors.InboxAdaptor;

public class ASTRAInboxAdaptor implements InboxAdaptor {
    private Agent agent;
    
    public ASTRAInboxAdaptor(Agent agent) {
        this.agent = agent;
    }

    @Override
    public boolean receive(JsonNode node) {
        AstraMessage message = new AstraMessage();
        message.performative = node.get("performative").asText();
        message.sender = node.get("sender").asText();
        
        // Should replace ObjectSerialisation to enable cross platform messaging...
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(node.get("content").binaryValue()));
            message.content = in.readObject();
            in.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        // ReIndex the content in case it is from another platform (indexing is platform
        // dependent).
        // NOTE: Indexing is the process of mapping strings to integers to speed matching
        //       of predicates, functions and other terms.
        if (Formula.class.isInstance(message.content)) Formula.class.cast(message.content).reIndex();

        // Send it the new way...
        agent.receive(message);
        return true;
    }

}