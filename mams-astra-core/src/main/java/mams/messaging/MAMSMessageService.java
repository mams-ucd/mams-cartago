package mams.messaging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import astra.core.Agent;
import astra.messaging.AstraMessage;
import astra.messaging.MessageService;
import mams.web.RequestObject;
import mams.web.WebResponse;
import mams.web.WebUtils;

public class MAMSMessageService extends MessageService {
    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    }

    private boolean logging = false;

    @Override
    public boolean sendMessage(AstraMessage astraMessage) {
        if (logging) 
            System.out.println(
                "[MessageService] SENDING: " + astraMessage.performative + 
                " from: " + astraMessage.sender + 
                " to: " + astraMessage.receivers + 
                " with: " + astraMessage.content
            );

        ArrayNode receivers = mapper.createArrayNode();
        for (String receiver : astraMessage.receivers) {
            receivers.add(receiver);
        }

        ObjectNode message = mapper.createObjectNode();
        message.put("sender", astraMessage.sender);
        message.set("receivers", receivers);
        message.put("performative", astraMessage.performative);

		try {
            ByteArrayOutputStream baout = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(baout);
            out.writeObject(astraMessage.content);
            byte[] buf = baout.toByteArray();
            message.put("content", buf);
            out.close();
            baout.close();
        } catch (IOException e1) {
            System.out.println("[MessageService] FAILED to serialise content: " + astraMessage.content);
            e1.printStackTrace();
            return false;
        }

        for (String receiver : astraMessage.receivers) {
            // message.put("receiver", receiver);
            if (!receiver.startsWith("http")) {
                if (logging)
                    System.out.println("[MessageService] Receiver is not a URI, SENDING LOCALLY TO: " + receiver);

                // Try local connection
                Agent agent = Agent.getAgent(receiver);
                if (agent != null) {
                    agent.receive(astraMessage);
                } else {
                    System.out.println("[MessageService] FAILED to deliver (LOCALLY) message: " + astraMessage);
                    System.out.println("[MessageService] To: " + receiver);
                }
            } else {
                RequestObject object = new RequestObject();
                object.url = receiver+"/inbox";
                object.method = "POST";
                object.type = "application/json";
                try {
                    object.content = mapper.writeValueAsString(message);
                } catch (JsonProcessingException e) {
                    System.out.println("[MessageService] FAILED to serialize message: " + astraMessage);
                    e.printStackTrace();
                    return false;
                }
        
    
                WebResponse response = WebUtils.sendRequest(object);
                if (response.getCode() != 200) {
                    System.out.println("[MessageService] FAILED to deliver message: " + astraMessage);
                    System.out.println("[MessageService] Got " + response.getCode() + " response.");
                }
            }
        }

        return true;
    }

    @Override
    public void configure(String key, String value) {
        switch (key) {
            case "logging":
                logging = value.equals("on");
                break;
            default:
                System.err.println("[LocalMQService] Unknown propery: " + key);
        } 
    }

    @Override
    public void start() {}
    
}
