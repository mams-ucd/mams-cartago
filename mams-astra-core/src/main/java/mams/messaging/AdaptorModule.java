package mams.messaging;

import astra.core.Module;
import mams.adaptors.InboxAdaptor;

public class AdaptorModule extends Module {
    @TERM
    public InboxAdaptor getInboxAdaptor() {
        return new ASTRAInboxAdaptor(agent);
    }
}
