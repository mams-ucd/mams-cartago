import static org.junit.Assert.assertTrue;

import org.junit.Test;

import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import mams.HALConverter;

public class HALConverterTest {
    private HALConverter hal = new HALConverter();
    
    private Funct getFunct(Funct funct, int index) {
        return (Funct) funct.termAt(index);
    }

    private Primitive<?> getPrimitive(Funct funct, int index) {
        return (Primitive<?>) funct.termAt(index);
    }

    private ListTerm getList(Funct funct, int index) {
        return (ListTerm) funct.termAt(index);
    }

    @Test
    public void oneFieldTest() {
        Funct funct = hal.toRawFunct("test", "{\"param\":\"value\"}");
        assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(param(\"value\"))"));
        String param = getFunct(funct, 0).functor();
        Object value = getPrimitive(getFunct(funct, 0), 0).value();
        assertTrue("Incorrect Param: " + param, param.equals("param"));
        assertTrue("Incorrect Value: " + value, value.equals("value"));
    }

    @Test
    public void twoFieldTest() {
        Funct funct = hal.toRawFunct("test", "{\"param\":\"value\", \"param2\":\"value2\"}");
        assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(param(\"value\"),param2(\"value2\"))"));
        String param = getFunct(funct, 0).functor();
        Object value = getPrimitive(getFunct(funct, 0), 0).value();
        String param2 = getFunct(funct, 1).functor();
        Object value2 = getPrimitive(getFunct(funct, 1), 0).value();
        assertTrue("Incorrect Param: " + param, param.equals("param"));
        assertTrue("Incorrect Value: " + value, value.equals("value"));
        assertTrue("Incorrect Param 2: " + param2, param2.equals("param2"));
        assertTrue("Incorrect Value 2: " + value2, value2.equals("value2"));
    }

    @Test
    public void listTest() {
        Funct funct = hal.toRawFunct("test", "{\"param\":[\"value\", \"value2\"]}");
        assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(param([\"value\",\"value2\"]))"));
        String param = getFunct(funct,0).functor();
        ListTerm list = getList(getFunct(funct, 0), 0);
        assertTrue("Incorrect Param: " + param, param.equals("param"));
        assertTrue("Incorrect List Size: " + list, list.size() == 2);
    }

    @Test
    public void objectFieldTest() {
        Funct funct = hal.toRawFunct("test", "{\"field\": { \"param\":\"value\"}}");
        assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(field(field(param(\"value\"))))"));
    }

}