package mams;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;

/**
 * Example Usage:
 *
 * - JSON:
 *  { "name" : "rem", "address" : { "street": "rehoboth place"} , "items" : ["one", "two", "three"] }
 *
 * - ASTRA:
 *
 * import com.fasterxml.jackson.databind.JsonNode;
 *
 * agent Example {
 *   module JSONConverter converter;
 *   ...
 *   rule +!example(string json) {
 *     JsonNode node = converter.parse(json);
 *     string name = converter.valueAsString(node, "/name");
 *     string street = converter.valueAsString(node, "/address/street");
 *     int length = converter.length(node, "/address/items");
 *     string itemTwo = converter.valueAsString(node, "/address/items[1]");
 *   }
 * }
 */
public class JSONConverter2 extends Module {

    ObjectMapper objectMapper = new ObjectMapper();

    @TERM
    public JsonNode parse(String json){
        try{
            JsonNode node = objectMapper.readTree(json);
            // System.out.println("NODE: "+node);
            return node;
        } catch(JsonParseException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    //JsonNode node = json.parse(json-string)
    //json.valueAsString(node, )

    @TERM
    public String valueAsString(JsonNode node, String path){
        // System.out.println("in node: " + node);
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asText();
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, String value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asText().equals(value) ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, double value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asDouble() == value ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, int value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asInt() == value ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, long value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asLong() == value ? Predicate.TRUE:Predicate.FALSE;
    }

    @TERM
    public double valueAsDouble(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asDouble();
    }

    @TERM
    public long valueAsLong(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asLong();
    }

    @TERM
    public int length(JsonNode node, String path) {
        JsonNode jsonNode = getNode(node, path);
        // System.out.println("jsonNode:" + jsonNode);
        if (!jsonNode.isArray()) {
            throw new RuntimeException("Attempt to read length of JSON node that is not a list");
        }
        return jsonNode.size();
    }

    @TERM
    public JsonNode getNode(JsonNode node, String path){
        // System.out.println("node: " + node);
        // System.out.println("path: " + path);
        String split[] = path.split("/");
        JsonNode temp = node;
        for(String s: split){
            if(s.isEmpty()) continue;
            int index  = s.indexOf("[");

            if(index == -1){
                temp = temp.get(s);
            }else{
                if(index > 0){
                    String segment = s.substring(0, index);
                    temp = temp.get(segment); 
                }
                String rank = s.substring(index+1, s.length()-1);
                // System.out.println("rank: " + rank);
                // System.out.println("temp: " + temp);
                temp = temp.get(Integer.parseInt(rank));
                // System.out.println("temp: " + temp);
            }
        }
        return temp;
    }

    @TERM
    public String convertKeyValueToString(String key, String value){
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put(key, value);
        String json;
        try {
            json = objectMapper.writeValueAsString(objectNode);
            return json;
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
}
