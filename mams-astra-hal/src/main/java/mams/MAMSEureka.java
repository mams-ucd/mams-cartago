package mams;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cartago.OPERATION;
import mams.web.RequestObject;
import mams.web.WebResponse;
import mams.web.WebUtils;

public class MAMSEureka {

    @OPERATION
    public void getInstances(String uri){
        
        try {

            String constructedURI = "http://"; 

            WebResponse response = getRequest(uri);
            System.out.println("RESPONSE: "+response.getContent());
            Document doc = XMLParser.convertStringToXMLDocument(response.getContent());
            Element root = (Element) doc.getDocumentElement();
            String host = XMLParser.getString("ipAddr", root);
            String port = XMLParser.getString("management.port", root);
            // String host = XMLParser.getString("ipAddr", root);
            // String host = XMLParser.getString("ipAddr", root);

            
            
            System.out.println("Host: "+host);
            System.out.println("PORT: "+port);

            constructedURI += host + ":";
            constructedURI += port;
            System.out.println("CONSTRUCTED URI: "+constructedURI);
            System.out.println(response.getCode());

            // WebResponse webResponse = getRequest(constructedURI);

            

            // System.out.println(response.getContent());
        } catch (Throwable th) {
            th.printStackTrace();
            System.exit(0);
        }
    }

    private WebResponse getRequest(String uri){
        RequestObject requestObject = new RequestObject();
        requestObject.method="GET";
        requestObject.url=uri;
        requestObject.type="application/json";
        WebResponse response = WebUtils.sendRequest(requestObject);
        return response;
    }


        

    public static void main(String[] args) {
        MAMSEureka mamsEureka = new MAMSEureka();
        mamsEureka.getInstances("http://localhost:8761/eureka/apps");
    }
}
