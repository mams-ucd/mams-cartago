package mams.hal;

agent Passive extends mams.MAMSAgent {
    initial baseArtifact("mams.hal.artifacts.BaseJavaTypesArtifact");
    
	rule +!itemResource(string name, string cls) : ~have(name) & artifact("base", string baseName, cartago.ArtifactId baseId) {
        string resName = baseName+"-"+name;
		cartago.makeArtifact(resName, "mams.hal.artifacts.PassiveItemArtifact", cartago.params([name, cls]), cartago.ArtifactId id);
		cartago.linkArtifacts(id, "out-1", baseId);
		cartago.focus(id);
		cartago.operation(id, createRoute());
		+artifact(name, resName, id);
		+itemResource(resName, cls);
		cartago.operation(id, getUri(string uri));
	}

	rule +!listResource(string name, string cls) : ~have(name) & artifact("base", string baseName, cartago.ArtifactId id2) {
        string resName = baseName+"-"+name;
        cartago.makeArtifact(resName, "mams.hal.artifacts.PassiveListArtifact", cartago.params([name, cls]), cartago.ArtifactId id);
		cartago.linkArtifacts(id, "out-1", id2);
		cartago.focus(id);
		cartago.operation(id, createRoute());

		// Store URI of artifact
        cartago.operation(id, getUri(string uri));
		+uri(resName, uri);

        +artifact(name, resName, id);
		+listResource(name, cls);
	}
	
    rule +!monitorPassiveItem(string type, string qname) {
        cartago.lookupArtifact(qname, cartago.ArtifactId id);
        cartago.focus(id);
		
        // Record info about the artifact
        +artifact(qname, qname, id);
    }

    rule $cartago.signal(string source_artifact_name, listItemArtifactCreated(string artifact_name, string type)) {
        cartago.lookupArtifact(artifact_name, cartago.ArtifactId id);
        cartago.focus(id);

        // Record info about the artifact
        +artifact(artifact_name, artifact_name, id);
        +itemResource(artifact_name, type);
	}

	rule +itemResource(string artifact_name, string type) {
		cartago.println("New item resource: " + artifact_name + " of type: " + type);
	}

    rule +!getObject(string artifact_name, Object obj)
        : artifact(artifact_name, artifact_name, cartago.ArtifactId id) {
        cartago.operation(id, get(obj));
    }

    rule +!updateObject(string artifact_name, Object obj)
        : artifact(artifact_name, artifact_name, cartago.ArtifactId id) {
        cartago.operation(id, update(obj));
    }
}