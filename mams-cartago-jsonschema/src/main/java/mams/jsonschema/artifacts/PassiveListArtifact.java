package mams.jsonschema.artifacts;

import com.fasterxml.jackson.databind.JsonNode;

import cartago.ARTIFACT_INFO;
import cartago.ArtifactId;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.Op;
import cartago.OpFeedbackParam;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.Handler;
import mams.web.ResponseEntity;



@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListArtifact extends PassiveArtifact {
    private static int COUNTER = 0;
    {
        set(HttpMethod.GET, context -> {
            try {
                ResourceArtifact[] children = getChildren();
                Object[] array = new Object[children.length];
                int i = 0;
                for (ResourceArtifact artifact : children) {
                    array[i++] =((TypedResourceArtifact) artifact).getData();
                }
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(array));
                
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        set(HttpMethod.POST, context -> {
            try {
                Object data = mapper.readTree(Utils.getBody(context.getRequest()));
                if (data == null) System.exit(0);

                String slug = context.getRequest().headers().get("Slug");
                if (slug == null) {
                    slug = name+(COUNTER++);
                    System.out.println("["+name+".POST] No slug provided, using default: " + slug);
                }
                // String name = Utils.getIdentifier(className, data);

                if (handler.getLinks().contains(slug)) {
                    return ResponseEntity.type("application/json").status(HttpResponseStatus.CONFLICT);
                }

                OpFeedbackParam<ArtifactId> id = new OpFeedbackParam<ArtifactId>();
                String qname = getId().getName()+"-"+slug;
                CartagoBackend.getInstance().
                    doAction(new Op("makeArtifact", qname, "mams.javatypes.artifacts.PassiveListItemArtifact", new Object[] {slug, getSchema(), data}, id));
    
                CartagoBackend.getInstance().doAction(new Op("linkArtifacts", id.get(), "out-1", getId()));
                CartagoBackend.getInstance().doAction(id.get(), new Op("createRoute"));
                String uri = getUri() + "/" + slug;
                return ResponseEntity.type("application/json").header("Location", uri).status(HttpResponseStatus.CREATED);
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }

    @OPERATION
	void init(final String name, final JsonNode schema) {
        this.name = name;
        this.defineObsProperty(SCHEMA, schema);
    }

    protected int size() {
        return this.handler.getLinks().size();
    }

    @OPERATION
    public void size(final OpFeedbackParam<Integer> size) {
        size.set(size());
    }

    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        super.attach(id, childHandler, baseUri);
        this.signal("listItemArtifactCreated", getId().getName()+"-"+id, getSchema());
    }

	@LINK
	public void detach(String id){
		System.out.println("[" + getId() + "] Detaching from list: " + id);
		handler.deleteRoute(id);
	}

}


