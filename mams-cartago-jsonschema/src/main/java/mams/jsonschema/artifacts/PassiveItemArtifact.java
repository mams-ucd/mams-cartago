package mams.jsonschema.artifacts;

import com.fasterxml.jackson.databind.JsonNode;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import cartago.Op;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.ResponseEntity;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveItemArtifact extends PassiveArtifact {
    {
        set(HttpMethod.PUT, context -> {
            try {
                String content = Utils.getBody(context.getRequest());
                JsonNode data = mapper.readTree(content);
                if (this.validateJson(data)) {
                    CartagoBackend
                        .getInstance()
                        .doAction(
                            getId(), 
                            new Op("replace", data));
                    return ResponseEntity.type("application/json").status(HttpResponseStatus.OK);
                } else {
                    return ResponseEntity.type("text/plain").status(HttpResponseStatus.UNPROCESSABLE_ENTITY).body("{ 'error':'Could not validate', 'data':" + content + "}");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        set(HttpMethod.PATCH, context -> {
            try {
                CartagoBackend
                    .getInstance()
                    .doAction(
                        getId(), 
                        new Op("update", mapper.readTree(Utils.getBody(context.getRequest()))));
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }
}