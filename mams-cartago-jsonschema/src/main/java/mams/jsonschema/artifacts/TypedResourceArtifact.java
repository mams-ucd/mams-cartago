package mams.jsonschema.artifacts;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import cartago.OPERATION;
import cartago.OperationException;
import mams.artifacts.ResourceArtifact;
import mams.handlers.ResourceHandler;
import mams.jsonschema.handlers.JsonSchemaHandler;

public abstract class TypedResourceArtifact extends ResourceArtifact {
    protected static final String SCHEMA = "schema";
    protected static final String DATA = "data";
    
    protected static ObjectMapper mapper = new ObjectMapper();
    private static JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    private JsonSchema schema;

	public ResourceArtifact[] getChildren() {
		return handler.getChildren();
	}

	@OPERATION void init(String name, JsonNode schema) {
        this.name = name;
        
        try {
            this.schema = factory.getJsonSchema(schema);
        } catch (ProcessingException e) {
            e.printStackTrace();
        }

        this.defineObsProperty(SCHEMA, schema);
        this.defineObsProperty(DATA, mapper.createObjectNode());
    }

    @OPERATION void init(String name, JsonNode schema, JsonNode data) {
        this.name = name;

        this.defineObsProperty(SCHEMA, schema);
        this.defineObsProperty(DATA, data);
    }

    @OPERATION public void replace(JsonNode data) throws Exception {
        this.updateObsProperty(DATA, data);
        signal("replaced", data);
    }

    public boolean validateJson(JsonNode data) {
        ProcessingReport report;

        try {
            report = schema.validate(data,true);
        } catch (ProcessingException e) {
            System.out.println("[" + getId() + "] Validation Error for: " + data);
            return false;
        }

        return report.isSuccess();
    }

    public JsonNode getData() {
        return (JsonNode) getObsProperty(DATA).getValue();
    }

    public JsonNode getSchema() {
        return (JsonNode) getObsProperty(SCHEMA).getValue();
    }

    @OPERATION void destroyArtifact(){
        System.out.println("[" + getId()+ "] Destroy Artifact Called...");
        try {
			execLinkedOp("out-1", "detach", this.name);
		} catch (OperationException e) {
			e.printStackTrace();
		}
        System.out.println("[" + getId()+ "] Destroy Artifact Completed...");
    }

    protected ResourceHandler createHandler() {
        return new JsonSchemaHandler(this);
    }
}
