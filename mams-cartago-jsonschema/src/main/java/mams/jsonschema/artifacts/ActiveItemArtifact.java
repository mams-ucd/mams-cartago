package mams.jsonschema.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.web.ResponseEntity;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class ActiveItemArtifact extends ActiveArtifact {
    {
        set(HttpMethod.PUT, event -> {
            try {
                replace(mapper.readTree(new String(event.requestBody, "UTF-8")));
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        // System.out.println("ADDED PUT");
    }
    
}