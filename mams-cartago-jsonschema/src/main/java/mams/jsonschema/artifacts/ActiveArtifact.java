package mams.jsonschema.artifacts;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import cartago.OPERATION;
import cartago.Op;
import cartago.OpFeedbackParam;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.jsonschema.model.HttpEvent;
import mams.utils.CartagoBackend;
import mams.utils.HttpContext;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class ActiveArtifact extends TypedResourceArtifact {
    private static int INDEX = 0;
    private Map<Integer, HttpEvent> events = new TreeMap<Integer, HttpEvent>();
    private Map<HttpMethod, Function<HttpEvent, ResponseEntity>> methods = new HashMap<>();

    {
        set(HttpMethod.OPTIONS, event -> {
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (HttpMethod method : methods.keySet()) {
                if (first) first=false; else builder.append(",");
                builder.append(method.asciiName());
            }
            return new ResponseEntity(HttpResponseStatus.NO_CONTENT).header("Allow", builder.toString());
        });
        set(HttpMethod.GET, event -> {
            try {
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getData()));
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        // System.out.println("ADDED GET,HEAD");
    }

    protected void set(HttpMethod method, Function<HttpEvent,ResponseEntity> context) {
        methods.put(method, context);
    }

    @OPERATION public void accept(int id) {
        HttpEvent event = events.remove(id);
        
        WebServer.writeResponse(
                event.context.getCtx(),
                event.context.getRequest(),
                event.method.apply(event));
    }

    @OPERATION public void reject(int id) {
        HttpEvent event = events.remove(id);

        WebServer.writeErrorResponse(
                event.context.getCtx(),
                event.context.getRequest(),
                HttpResponseStatus.FORBIDDEN);
    }

    @OPERATION public void httpEvent(int id, String method) {
        // System.out.println(">>>>>> EVENT: " + id + "/" + method + "["+getId()+"]");
        signal("httpEvent", id, method);
    }

    @OPERATION public void jsonBody(int index, OpFeedbackParam<JsonNode> node) throws JsonMappingException, JsonProcessingException, ClassNotFoundException, IOException {
        HttpEvent event = events.get(index);
        node.set(mapper.readTree(new String(event.requestBody, "UTF-8")));
    }

    @OPERATION public void header(int index, String header, OpFeedbackParam<String> value) {
        HttpEvent event = events.get(index);
        String val = event.context.getRequest().headers().get(header);
        if (val != null) value.set(val); else value.set("");
    }

    public boolean handle(ChannelHandlerContext ctx, FullHttpRequest request) {
        Function<HttpEvent, ResponseEntity> method = methods.get(request.method());

        if (method == null) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            int index = INDEX++;

            ByteBuf buf = request.content();
            byte[] bytes = new byte[buf.readableBytes()];
            buf.readBytes(bytes);
            events.put(index, new HttpEvent(new HttpContext(request, ctx), method, bytes));
            try {
                CartagoBackend
                    .getInstance()
                    .doAction(getId(), new Op("httpEvent", index, request.method().toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }    
}