package mams.jsonschema.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.ArtifactConfig;
import cartago.ArtifactId;
import cartago.LINK;
import cartago.OUTPORT;
import cartago.Op;
import cartago.OpFeedbackParam;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.web.Handler;
import mams.web.ResponseEntity;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class ActiveListArtifact extends ActiveArtifact {
    private static int COUNTER = 0;

    {
        set(HttpMethod.GET, event -> {
            try {
                ResourceArtifact[] children = getChildren();
                Object[] array = new Object[children.length];
                int i = 0;
                for (ResourceArtifact artifact : children) {
                    array[i++] =((TypedResourceArtifact) artifact).getData();
                }
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(array));
                
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        set(HttpMethod.POST, event -> {
            try {

                Object data = mapper.readTree(new String(event.requestBody, "UTF-8"));
                if (data == null) System.exit(0);

                String slug = event.context.getRequest().headers().get("Slug");
                if (slug == null) {
                    // Check if the java class has an identifier specified
                    slug = name+(COUNTER++);
                    System.out.println("["+name+".POST] No slug provided, using default: " + slug);
                }
                // String name = Utils.getIdentifier(className, data);

                if (handler.getLinks().contains(slug)) {
                    return ResponseEntity.type("application/json").status(HttpResponseStatus.CONFLICT);
                }

                String qname = getId().getName()+"-"+slug;
                ArtifactId aid = this.makeArtifact(qname, "mams.javatypes.artifacts.ActiveListItemArtifact", new ArtifactConfig(slug, getSchema(), data));
                CartagoBackend.getInstance().doAction(new Op("linkArtifacts", aid, "out-1", getId()));
                this.execLinkedOp(aid, "createRoute");
                String uri = getUri() + "/" + slug;

                return ResponseEntity.type("application/json").header("Location", uri).status(HttpResponseStatus.CREATED);
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        // System.out.println("ADDED POST,GET");
    }

    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        super.attach(id, childHandler, baseUri);
        this.signal("listItemArtifactCreated", getId().getName()+"-"+id, getSchema());
    }

	@LINK
	public void detach(String id){
		System.out.println("[" + getId() + "] Detaching from list: " + id);
		handler.deleteRoute(id);
	}
}