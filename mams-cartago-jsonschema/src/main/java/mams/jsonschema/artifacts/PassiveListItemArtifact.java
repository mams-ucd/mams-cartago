package mams.jsonschema.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import cartago.Op;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.utils.CartagoBackend;
import mams.web.ResponseEntity;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListItemArtifact extends PassiveItemArtifact {
    {
        set(HttpMethod.DELETE, context -> {
            try {
                CartagoBackend.getInstance().doAction(getId(), new Op("destroyArtifact"));
                CartagoBackend.getInstance().doAction(new Op("disposeArtifact", getId()));

                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getData()));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }
}
