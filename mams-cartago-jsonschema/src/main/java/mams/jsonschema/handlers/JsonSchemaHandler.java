package mams.jsonschema.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.handlers.ResourceHandler;
import mams.jsonschema.artifacts.TypedResourceArtifact;
import mams.web.Handler;
import mams.web.Path;
import mams.web.WebServer;

public class JsonSchemaHandler extends ResourceHandler {
    public JsonSchemaHandler(TypedResourceArtifact artifact) {
        super(artifact);
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        this.path = path;
        if (this.path.length() > 0) {
            Handler handler = routes.get(path.prefix());
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, this.path);
            }
        } else {
            artifact.handle(ctx, request);
        }
	}
}

