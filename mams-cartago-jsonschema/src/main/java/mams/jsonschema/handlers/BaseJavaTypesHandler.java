package mams.jsonschema.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.BaseArtifact;
import mams.handlers.BaseHandler;
import mams.jsonschema.Constants;
import mams.web.Handler;
import mams.web.Path;
import mams.web.WebServer;

public class BaseJavaTypesHandler extends BaseHandler {
    public BaseJavaTypesHandler(BaseArtifact artifact, String context) {
        super(artifact, context);
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        if (path.length() == 0) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.FORBIDDEN);
        }  else {
            String prefix = path.prefix();

            if (prefix.equals(Constants.PROFILE)) {
                System.out.println("SPECIAL CASE");

                // This would be a hook to display the json schema for the resource
            }
            // System.out.println("Routes: " + routes.keySet());
            // System.out.println("Classes: " + routes.values());
            // System.out.println("Prefix: " + prefix);
            Handler handler = routes.get(prefix);
            
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, path);
            }
        }
	}
}

