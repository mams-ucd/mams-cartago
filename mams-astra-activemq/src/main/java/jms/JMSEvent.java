package jms;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class JMSEvent implements Event {
	Term type;
	Term source;
	Term content;
	
	public JMSEvent(Term type, Term source, Term content) {
		this.type = type;
		this.source = source;
		this.content = content;
	}
	
	public Object getSource() {
		return null;
	}

	public String signature() {
		return "$jms:";
	}

	public Event accept(LogicVisitor visitor) {
		return new JMSEvent((Term) type.accept(visitor), (Term) source.accept(visitor), (Term) content.accept(visitor));
	}
}
