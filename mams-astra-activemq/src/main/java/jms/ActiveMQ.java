package jms;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

import astra.core.Module;
import astra.event.Event;
import astra.reasoner.Unifier;
import astra.term.Primitive;
import astra.term.Term;

public class ActiveMQ extends Module {
	static {
		Unifier.eventFactory.put(JMSEvent.class, new JMSEventUnifier());
	}
    public static final String TOPIC = "TOPIC";
    public static final String QUEUE = "QUEUE";

    public static final Primitive<String> TOPIC_TYPE = Primitive.newPrimitive(TOPIC);
    public static final Primitive<String> QUEUE_TYPE = Primitive.newPrimitive(QUEUE);

    private static class AMQTuple {
        Session session;
        MessageConsumer consumer;

        AMQTuple(Session session, MessageConsumer consumer) {
            this.session = session;
            this.consumer = consumer;
        }
    }

    private Connection connection;
    private Map<String,Map<String,AMQTuple>> consumers = new HashMap<>();

    {
        consumers.put(TOPIC, new HashMap<>());
        consumers.put(QUEUE, new HashMap<>());
    }

    @ACTION
    public boolean initialize(String host) {
        return initialize(host, agent.name());
    }

    @ACTION
    public boolean initialize(String host, String clientName) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("failover://tcp://"+host+":61616");
        try {
            connection = connectionFactory.createConnection();
            connection.setClientID(clientName);
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean start() {
        try {
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean subscribe_topic(String topicName) {
        try {
            Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            Topic topic = session.createTopic(topicName);
            MessageConsumer consumer = session.createConsumer(topic);
            consumers.get(TOPIC).put(topicName, new AMQTuple(session, consumer));
            consumer.setMessageListener(new MessageListener() {
                public void onMessage(Message message) {
                    try {
                        String json = ((TextMessage) message).getText();
                        agent.addEvent(new JMSEvent(TOPIC_TYPE, Primitive.newPrimitive(topicName), Primitive.newPrimitive(json)));
                        message.acknowledge();
                    } catch (JMSException e) {
                        System.out.println("[ActiveMQ.WARNING] Unhandled message:\n" + message.toString());
                    }
                }
            });

        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean subscribe_queue(String name) {
        try {
            Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            Queue queue = session.createQueue(name);
            MessageConsumer consumer = session.createConsumer(queue);
            consumers.get(QUEUE).put(name, new AMQTuple(session, consumer));
            consumer.setMessageListener(new MessageListener() {
                public void onMessage(Message message) {
                    try {
                        String json = ((TextMessage) message).getText();
                        agent.addEvent(new JMSEvent(QUEUE_TYPE, Primitive.newPrimitive(name), Primitive.newPrimitive(json)));
                        message.acknowledge();
                    } catch (JMSException e) {
                        System.out.println("[ActiveMQ.WARNING] Unhandled message:\n" + message.toString());
                    }
                }
            });

        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean unsubscribe_topic(String topic) {
        AMQTuple tuple = consumers.get(TOPIC).get(topic);
        try {
            tuple.consumer.setMessageListener(null);
            tuple.consumer.close();
            tuple.session.close();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    

    @ACTION
    public boolean unsubscribe_queue(String queue) {
        AMQTuple tuple = consumers.get(QUEUE).get(queue);
        try {
            tuple.consumer.setMessageListener(null);
            tuple.consumer.close();
            tuple.session.close();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    @ACTION
    public boolean publish_to_topic(String topicName, String text) {
        try {
            Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            Topic topic = session.createTopic(topicName);
            MessageProducer producer = session.createProducer(topic);
                
            TextMessage textMessage = session.createTextMessage(text);
            producer.send(textMessage);

            producer.close();
            session.close();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @ACTION
    public boolean publish_to_queue(String name, String text) {
        try {
            Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            Queue queue = session.createQueue(name);
            MessageProducer producer = session.createProducer(queue);

            TextMessage textMessage = session.createTextMessage(text);
            producer.send(textMessage);

            producer.close();
            session.close();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

	@EVENT( types = { "string", "string", "string" }, signature="$jms:", symbols = {} )
	public Event event(Term type, Term source, Term content) {
		return new JMSEvent(type, source, content);
	}

}