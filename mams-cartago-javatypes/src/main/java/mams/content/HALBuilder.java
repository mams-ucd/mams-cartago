package mams.content;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class HALBuilder {
    private ObjectMapper mapper = new ObjectMapper();
    private ObjectNode rootNode;
    private ObjectNode linksNode;
    private ObjectNode embeddedNode;
    private ArrayNode bodyArray;

    public HALBuilder() {
        this.rootNode = mapper.createObjectNode();
        this.linksNode = mapper.createObjectNode();
        this.embeddedNode = mapper.createObjectNode();
        this.bodyArray = mapper.createArrayNode();
    }

    public HALBuilder addParentLink(String href) {
        return addLink("parent", href);
    }

    public HALBuilder addSelfLink(String href) {
        return addLink("self", href);
    }

    public HALBuilder addProfileLink(String href) {
        ObjectNode link = mapper.createObjectNode();
        link.put("href", href);
        link.put("mediaTypes", "jsonSchema");
        linksNode.set("profile", link);
        return this;
    }

    private HALBuilder addLink(String rel, String href) {
        ObjectNode link = mapper.createObjectNode();
        link.put("href", href);
        linksNode.set(rel, link);
        return this;
    }

    public HALBuilder addArtifact(String selfHref, String model, String type) {
        ObjectNode artifactNode = mapper.createObjectNode();
        ObjectNode artifactLinks = mapper.createObjectNode();
        ObjectNode artifactSelfLink = mapper.createObjectNode();
        artifactSelfLink.put("href", selfHref);
        artifactLinks.set("self", artifactSelfLink);
        artifactNode.set("_links", artifactLinks);
        artifactNode.put("model", model);
        artifactNode.put("type", type);
        bodyArray.add(artifactNode);
        return this;
    }

    public HALBuilder setName(String name) {
        rootNode.put("name", name);
        return this;
    }

    public HALBuilder setType(String type) {
        rootNode.put("type", type);
        return this;
    }

    public String build() throws JsonProcessingException {
        rootNode.set("_links", linksNode);
        embeddedNode.set("body", bodyArray);
        rootNode.set("_embedded", embeddedNode);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
    }

    public static HALBuilder create() {
        return new HALBuilder();
    }
}
