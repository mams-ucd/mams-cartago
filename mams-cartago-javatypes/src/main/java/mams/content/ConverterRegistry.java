package mams.content;

import java.util.HashMap;
import java.util.Map;

public class ConverterRegistry {
    private static final Map<String, Converter> converters = new HashMap<String, Converter>();

    static {
        converters.put("application/json", new JsonConverter());
        converters.put("application/hal+json", new HalConverter());
        converters.put("application/rdf", new RdfConverter());
    }

    public static Converter getConverter(String contentType) {
        return converters.getOrDefault(contentType, converters.get("application/json"));
    }
}