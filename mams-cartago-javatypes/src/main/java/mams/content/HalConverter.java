package mams.content;

import com.fasterxml.jackson.core.JsonProcessingException;

import mams.artifacts.ResourceArtifact;

public class HalConverter implements Converter {
    @Override
    public String convert(ConversionContext ctx) {
        try {
            return getHalRepresentation(ctx.getRequest().uri(), ctx.getChildren(), ctx.isChild());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "{\"error\":\"Error converting to HAL\"}";
        }
    }

    private String getHalRepresentation(String uri, ResourceArtifact[] children, boolean isChild) throws JsonProcessingException {
        
        HALBuilder halBuilder = HALBuilder.create()
            .addSelfLink(uri)
            .addProfileLink(uri + "/profile")
            .setName("main")
            .setType("ASTRA");

        if (isChild) halBuilder.addParentLink(uri.replaceFirst("/[^/]+$", "")); // remove last part of URI

        for (ResourceArtifact artifact : children) {
            // TODO: Need to understand where descriptorString() comes from...
            // System.out.println("Artifact: " + artifact.getClass().descriptorString());
            System.out.println("Artifact: " + artifact.getClass().toString());
            // Add each artifact to the HAL builder
            halBuilder.addArtifact(
                artifact.getUri(),
                artifact.getClass().getSimpleName(),
                "javatypes"
            );
        }

        return halBuilder.build();
    }
}
