package mams.content;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator;

import java.io.IOException;

public class JsonSchemaConverter {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final JsonSchemaGenerator schemaGen = new JsonSchemaGenerator(mapper);


    public static String javaToSchema(Class<?> cls) {
        try {
            JsonSchema schema = schemaGen.generateSchema(cls);

            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schema);
        } catch (IOException e) {
            System.err.println("Error mapping JSON to schema");
            e.printStackTrace();
            return "{}";
        }
    }

    public static String getSchemaForList(Class<?> cls) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode schemaNode = mapper.createObjectNode();
        
        // Set type as array
        schemaNode.put("type", "array");
        
        // Get schema for first child since all children are of same type
        try {
            String childSchema = JsonSchemaConverter.javaToSchema(cls);
            JsonNode itemsSchema = mapper.readTree(childSchema);
            schemaNode.set("items", itemsSchema);
            
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaNode);
        } catch (Exception e) {
            e.printStackTrace();
            return "{}";
        }
    }
}
