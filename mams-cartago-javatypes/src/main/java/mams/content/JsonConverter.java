package mams.content;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mams.artifacts.ResourceArtifact;
import mams.javatypes.artifacts.TypedResourceArtifact;

public class JsonConverter implements Converter {
    protected static ObjectMapper mapper = new ObjectMapper();

    @Override
    public String convert(ConversionContext ctx) {
        ResourceArtifact[] children = ctx.getChildren();
        Object[] array = new Object[children.length];
        int i = 0;
        for (ResourceArtifact artifact : children) {
            array[i++] =((TypedResourceArtifact) artifact).getObject();
        }
        try {
            if (array.length > 0) { // it is a list artifact
                return mapper.writeValueAsString(array);
            } else {
                return mapper.writeValueAsString(((TypedResourceArtifact) ctx.getArtifact()).getObject());
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "{\"type\":\"Json\",\"uri\":\"" + ctx.getRequest().uri() + "\"}";
    }
}
