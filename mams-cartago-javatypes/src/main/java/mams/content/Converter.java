package mams.content;

public interface Converter {
    String convert(ConversionContext ctx);
}
