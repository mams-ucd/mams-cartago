package mams.content;

import cartago.Artifact;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.ResourceArtifact;

public class ConversionContext {
    private Artifact artifact;
    private FullHttpRequest request;
    private ResourceArtifact[] children;
    private boolean isChild;
    
    public ConversionContext(Artifact artifact, FullHttpRequest request, ResourceArtifact[] children) {
        this(artifact, request, children, false);
    }

    public ConversionContext(Artifact artifact, FullHttpRequest request, ResourceArtifact[] children, boolean isChild) {
        this.artifact = artifact;
        this.request = request;
        this.children = children;
        this.isChild = isChild;
    }

    public Artifact getArtifact() { return artifact; }
    public FullHttpRequest getRequest() { return request; }
    public ResourceArtifact[] getChildren() { return children; }
    public boolean isChild() { return isChild; }
}