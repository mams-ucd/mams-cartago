package mams.content;

public class RdfConverter implements Converter {
    
    @Override
    public String convert(ConversionContext ctx) {
        // Build RDF representation here
        //return createRdfRepresentation(agentBody);

        return "{\"type\":\"Rdf\",\"value\":\"" + ctx.getRequest().uri() + "\"}";
    }
}
