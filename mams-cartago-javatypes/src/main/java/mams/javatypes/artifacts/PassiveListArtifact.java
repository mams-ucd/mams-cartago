package mams.javatypes.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.ArtifactId;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.Op;
import cartago.OpFeedbackParam;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.Handler;
import mams.web.ResponseEntity;



@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListArtifact extends PassiveArtifact {
    private static int COUNTER = 0;
    {
        set(HttpMethod.POST, context -> {
            try {
                Object data = mapper.readValue(Utils.getBody(context.getRequest()), Class.forName(className));
                if (data == null) System.exit(0);

                String slug = context.getRequest().headers().get("Slug");
                if (slug == null) {
                    // Check if the java class has an identifier specified
                    slug = Utils.getIdentifier(className, data);
                    if (slug == null) {
                        slug = name+(COUNTER++);
                        System.out.println("["+name+".POST] No slug provided, using default: " + slug);
                    }
                }

                if (handler.getLinks().contains(slug)) {
                    return ResponseEntity.type("application/json").status(HttpResponseStatus.CONFLICT);
                }

                OpFeedbackParam<ArtifactId> id = new OpFeedbackParam<ArtifactId>();
                String qname = getId().getName()+"-"+slug;
                CartagoBackend.getInstance().
                    doAction(new Op("makeArtifact", qname, "mams.javatypes.artifacts.PassiveListItemArtifact", new Object[] {slug, className, data}, id));
    
                CartagoBackend.getInstance().doAction(new Op("linkArtifacts", id.get(), "out-1", getId()));
                CartagoBackend.getInstance().doAction(id.get(), new Op("createRoute"));
                String uri = getUri() + "/" + slug;
                return ResponseEntity.type("application/json").header("Location", uri).status(HttpResponseStatus.CREATED);
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }

    @OPERATION
	void init(final String name, final String className) {
        this.name = name;
        this.className = className;
    }

    protected int size() {
        return this.handler.getLinks().size();
    }

    @OPERATION
    public void size(final OpFeedbackParam<Integer> size) {
        size.set(size());
    }

    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        super.attach(id, childHandler, baseUri);
        this.signal("listItemArtifactCreated", getId().getName()+"-"+id, className);
    }

	@LINK
	public void detach(String id){
		System.out.println("[" + getId() + "] Detaching from list: " + id);
		handler.deleteRoute(id);
	}

}


