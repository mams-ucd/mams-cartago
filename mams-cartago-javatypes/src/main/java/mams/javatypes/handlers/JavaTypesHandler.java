package mams.javatypes.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.content.JsonSchemaConverter;
import mams.javatypes.Constants;
import mams.javatypes.artifacts.TypedResourceArtifact;
import mams.handlers.ResourceHandler;
import mams.web.Handler;
import mams.web.Path;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class JavaTypesHandler extends ResourceHandler {
    public JavaTypesHandler(TypedResourceArtifact artifact) {
        super(artifact);
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        System.out.println("Path: "  +path);
        this.path = path;
        System.out.println("HERE...");
        if (this.path.length() > 0) {
            String prefix = this.path.prefix();
            if (prefix.equals(Constants.PROFILE)) {
                System.out.println("SPECIAL CASE");
                
                // Grab the class object from the artifact
                Class<?> cls = ((TypedResourceArtifact) this.artifact).getObject().getClass();
                
                String schema = "";
                // Check if the artifact is a list artifact
                if (artifact.getClass().getName().endsWith("ListArtifact")) {
                    schema = JsonSchemaConverter.getSchemaForList(cls);
                } else {
                    // convert the class object to json schema
                    schema = JsonSchemaConverter.javaToSchema(cls);
                }

                WebServer.writeResponse(ctx, request,  ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(schema));
                return;
            }

            Handler handler = routes.get(prefix);
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, this.path);
            }
        } else {
            // System.out.println("Method: " + request.method().toString());
            // System.out.println("Uri: " + request.uri().toString());
            artifact.handle(ctx, request);
        }
	}
}
