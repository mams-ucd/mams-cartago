package mams.javatypes.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.BaseArtifact;
import mams.content.ConversionContext;
import mams.content.Converter;
import mams.content.ConverterRegistry;
import mams.javatypes.Constants;
import mams.handlers.BaseHandler;
import mams.web.Handler;
import mams.web.Path;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class BaseJavaTypesHandler extends BaseHandler {
    private ObjectMapper mapper = new ObjectMapper();

    public BaseJavaTypesHandler(BaseArtifact artifact, String context) {
        super(artifact, context);
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        if (path.length() == 0) {
            String acceptHeader = request.headers().get("Accept");
            Converter converter = ConverterRegistry.getConverter(acceptHeader);

            // Retrieve the base artifact/agent body
            ConversionContext conversionContext = new ConversionContext(artifact, request, getChildren());    

            // Use converter to get representation
            String representation = converter.convert(conversionContext);

            WebServer.writeResponse(
                ctx, request,
                ResponseEntity.type(acceptHeader).status(HttpResponseStatus.OK).body(representation)
            );
        }  else {
            String prefix = path.prefix();

            if (prefix.equals(Constants.PROFILE)) {
                System.out.println("SPECIAL CASE, base java types handler");

                // This would be a hook to display the json schema for the resource
                WebServer.writeResponse(ctx, request,  ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString("{}")));
                return;
            }
            System.out.println("Routes: " + routes.keySet());
            System.out.println("Classes: " + routes.values());
            System.out.println("Prefix: " + prefix);
            Handler handler = routes.get(prefix);
            
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, path);
            }
        }
	}

}
