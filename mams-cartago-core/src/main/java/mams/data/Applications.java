
package mams.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "versions__delta",
    "apps__hashcode",
    "application"
})
public class Applications implements Serializable
{

    @JsonProperty("versions__delta")
    private Integer versions__delta;
    @JsonProperty("apps__hashcode")
    private String appsHashcode;
    @JsonProperty("application")
    private List<Application> application;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 1418567277177561104L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Applications() {
    }

    /**
     * 
     * @param versionsDelta
     * @param application
     * @param appsHashcode
     */
    public Applications(Integer versions__delta, String appsHashcode, List<Application> application) {
        super();
        this.versions__delta = versions__delta;
        this.appsHashcode = appsHashcode;
        this.application = application;
    }

    @JsonGetter("versions__delta")
    public Integer getVersionsDelta() {
        return versions__delta;
    }

    @JsonSetter("versions__delta")
    public void setVersions__Delta(Integer versions__delta) {
        this.versions__delta = versions__delta;
    }

    @JsonGetter("apps__hashcode")
    public String getAppsHashcode() {
        return appsHashcode;
    }

    @JsonSetter("apps__hashcode")
    public void setAppsHashcode(String appsHashcode) {
        this.appsHashcode = appsHashcode;
    }

    @JsonGetter("application")
    public List<Application> getApplication() {
        return application;
    }

    @JsonSetter("application")
    public void setApplication(List<Application> application) {
        this.application = application;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Applications.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("versionsDelta");
        sb.append('=');
        sb.append(((this.versions__delta == null)?"<null>":this.versions__delta));
        sb.append(',');
        sb.append("appsHashcode");
        sb.append('=');
        sb.append(((this.appsHashcode == null)?"<null>":this.appsHashcode));
        sb.append(',');
        sb.append("application");
        sb.append('=');
        sb.append(((this.application == null)?"<null>":this.application));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
