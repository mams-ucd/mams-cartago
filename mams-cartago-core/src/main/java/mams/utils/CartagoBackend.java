package mams.utils;

import cartago.util.agent.*;

public class CartagoBackend {
    private static CartagoBasicContext ctx;
    
    public static CartagoBasicContext getInstance() {
        if (ctx == null) {
            ctx = new CartagoBasicContext("_backend", "main");
        }
        return ctx;
    }
}