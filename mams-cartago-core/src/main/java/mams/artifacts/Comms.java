package mams.artifacts;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cartago.Artifact;
import cartago.OPERATION;
import mams.web.RequestObject;
import mams.web.WebResponse;
import mams.web.WebUtils;

public class Comms extends Artifact {
    private static ObjectMapper mapper = new ObjectMapper();

    @OPERATION
    public void inform(String sender, String receiver, String content) {
        transmit("inform", sender, receiver, content);
    }

    @OPERATION
    public void transmit(String performative, String sender, String receiver, String content) {
        ObjectNode message = null;
        try {
            message = mapper.createObjectNode();
            message.put("sender", sender);
            message.put("receiver", receiver);
            message.put("performative", performative);
            message.set("content", mapper.readTree(content));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        RequestObject object = new RequestObject();
        object.url = receiver+"/inbox";
        object.method = "POST";
        object.type = "application/json";
        try {
            object.content = mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        WebResponse response = WebUtils.sendRequest(object);
        if (response.getCode() != 200) {
            throw new RuntimeException("Failed to send message - got " + response.getCode() + " response.");
        }
    }
}