package mams.artifacts;

import mams.web.Handler;
import mams.web.WebServer;
import mams.web.ResponseEntity;
import mams.handlers.StaticResourceHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import cartago.*;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;


public class StaticResourceArtifact extends Artifact {

    protected StaticResourceHandler handler;
    private String uri;
    protected String name;

    public String getUri() {
        return uri;
	}
	
	public ArtifactId getId() {
		return super.getId();
	}
	
	@OPERATION
	void getUri(OpFeedbackParam<String> uri) {
		uri.set(this.uri);
	}
	
    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
		System.out.println("Attaching: " + id);
		baseUri.set(this.uri);
        handler.addRoute(id, childHandler);
	}
	
	@LINK
	public void detach(String id){
		handler.deleteRoute(id);
	}

	@OPERATION
    void update(String name, Object value) {
        this.updateObsProperty(name, value);
	}
	
    @OPERATION
	void createRoute(String fileName) {
		fileName = "/" + fileName;
		uri = WebServer.getInstance().getBaseUrl() + fileName;
		System.out.println("Exposing Agent Static Resource @ Uri: " + uri);
		
        handler = createHandler();
		WebServer.getInstance().createContext(fileName, handler);
	}
	
	public boolean handle(ChannelHandlerContext ctx, FullHttpRequest request) {
		
		String fileName = request.uri().replaceAll("/", "");
		
		try {
			File f = FileUtils.getFile(getClass().getClassLoader()
  				.getResource(fileName)
  				.getPath());
			FileInputStream fis = new FileInputStream(f);

			FileNameMap fileNameMap = URLConnection.getFileNameMap();
			String mimeType = fileNameMap.getContentTypeFor(f.getName());
			
    		String data = IOUtils.toString(fis, "UTF-8");
			ResponseEntity re = ResponseEntity.type(mimeType);
			re.body(data);
			WebServer.writeResponse(ctx, request, re);
		} catch (IOException ex) {
			System.out.println("Unable to read in " + fileName);
			ex.printStackTrace();
			
		} catch (NullPointerException npe) {
			System.out.println("Unable to read in " + fileName);
			npe.printStackTrace();
		}
        
        return true;
	}

	public Object getProperty(String name) {
		return getObsProperty(name).getValue();
	}

	protected StaticResourceHandler createHandler(){
		return new StaticResourceHandler(this);
	}
}