package mams.artifacts;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.adaptors.DefaultInboxAdaptor;
import mams.adaptors.InboxAdaptor;
import mams.utils.Utils;
import mams.web.ResponseEntity;
import mams.web.WebServer;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class Inbox extends ResourceArtifact {
    private static ObjectMapper mapper = new ObjectMapper();
    static {
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    }

    private InboxAdaptor adaptor;

    {
        name = "inbox";
    }

    public void init() {
        adaptor = new DefaultInboxAdaptor(this);
    }

    public void init(InboxAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    
    @Override
    public boolean handle(ChannelHandlerContext ctxt, FullHttpRequest request) {
        if (!request.method().asciiName().toString().equals("POST")) {
            WebServer.writeErrorResponse(ctxt, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            try {
                JsonNode node = mapper.readTree(Utils.getBody(request));
                if (adaptor.receive(node)) {
                    WebServer.writeResponse(ctxt, request, ResponseEntity.type("application/json").status(HttpResponseStatus.OK));
                } else {
                    WebServer.writeResponse(ctxt, request, ResponseEntity.type("plain/text").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body("InboxAdaptor Error"));
                }
            } catch (Exception e) {
                WebServer.writeResponse(ctxt, request, ResponseEntity.type("plain/text").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage()));
                e.printStackTrace();
            }
        }

        return false;
    }

}