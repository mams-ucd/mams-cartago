package mams.artifacts;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cartago.Artifact;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import mams.data.Application;
import mams.data.ApplicationsWrapper;
import mams.web.RequestObject;
import mams.web.WebResponse;
import mams.web.WebUtils;

public class EurekaArtifact extends Artifact {

    @OPERATION
    public void eurekaHeartbeat(OpFeedbackParam<Integer> responseCode) {
        WebResponse response = getRequest("http://localhost:8761");
        // System.out.println("Eureka Heartbeat check response code: " + response.getCode());
        responseCode.set(response.getCode());
    }

    @OPERATION
    public void getInstanceURL(String uri, OpFeedbackParam<ArrayList<String>> returnUrls, OpFeedbackParam<ArrayList<String>> returnApplicationNames) {
            try {
                //TODO: Fix the issue with only one application - Deserializer needs to be able to handle a list of Application objects and a single Application object
                String constructedURI = "http://";
                WebResponse response = getRequest(uri);
                String xmlApps = response.getContent().trim();
                ApplicationsWrapper apps;
                ObjectMapper objectMapper = new ObjectMapper();
                apps = objectMapper.readValue(xmlApps,ApplicationsWrapper.class);
                ArrayList<Application> applications = (ArrayList<Application>) apps.getApplications().getApplication();

                ArrayList<String> urls = new ArrayList<>();
                ArrayList<String> applicationNames = new ArrayList<>();
                for (Application application : applications) {
                    // System.out.println("apps: "+application.getName());
                        constructedURI = "http://";
                        if(application.getInstance().getStatus().equalsIgnoreCase("UP")){
                            String host = application.getInstance().getIpAddr();
                            String port = application.getInstance().getPort().get$();
                            constructedURI += host + ":";
                            constructedURI += port;
                            applicationNames.add(application.getName());
                            urls.add(constructedURI);
                        }
                }
                returnApplicationNames.set(applicationNames);
                returnUrls.set(urls);    
            } catch (Exception e) {
                System.out.println("\nEureka Applications catch triggered.");
                e.printStackTrace();
            }
    }

    @OPERATION
    public void querySwaggerForURL(String url, String agentName){


        /* Swagger stuff below */
        try{
            /** Issue with docker network IP address*/

            String swaggerURI = url+"/api-docs"; 
            // String swaggerURI = "http://localhost:8082/api-docs";
            WebResponse swaggerResponse = getRequest(swaggerURI); 

            /** Check if there is no OAS Document */
            if(swaggerResponse.getCode() == 404){
                signal("swaggerSignalNoOAS",url, agentName);
            }
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(swaggerResponse.getContent()); 
            JsonNode pathsNode = rootNode.path("paths");

            /** Used to replace the Docker container IP address with localhost in order to test */
            String path = "";
            if(url.contains("137")){
                path = url.replaceAll("/.*?:", "//localhost:");
            }


            List<String> endPoints = getNodeKeys(pathsNode);
            // List<String> urls = new ArrayList<>();

            Map<String, String> routes = new HashMap<String,String>();

            traverse(pathsNode, endPoints, path, routes);

            for(Map.Entry<String, String> set: routes.entrySet()){
                signal("swaggerSignal", set.getKey(), set.getValue(), agentName);
            }  


        } catch(Exception e){
            e.printStackTrace();
        }
        // getUrlsForAllPaths(keys, url);
    }

    private String traverse(JsonNode root, List<String> endPoints, String url, Map<String, String> routes){

        if(root.isObject()){
            // System.out.println("JsonObjectNode");
            Iterator<String> fieldNames = root.fieldNames();
    
            while(fieldNames.hasNext()) {
                String fieldName = fieldNames.next();
                // System.out.println("EndPoint: "+endPoints.get(0));
                if(endPoints.contains(fieldName)){
                    // System.out.println("FieldName: "+fieldName);
                    JsonNode fieldValue = root.get(fieldName);
                    String path = url+fieldName;
                    routes.put(path, traverse(fieldValue, endPoints, url, routes));
                } else if(fieldName.equals("get")){
                    // System.out.println("Found a get");
                    JsonNode fieldValue = root.get(fieldName);
                    traverse(fieldValue, endPoints, url, routes);
                    return "get";
                } else if(fieldName.equals("post")){
                    // System.out.println("Found a post");
                    JsonNode fieldValue = root.get(fieldName);
                    traverse(fieldValue, endPoints, url, routes);
                    return "post";
                }
            }
        } else if(root.isArray()){
            // System.out.println("JsonArrayNode");
            ArrayNode arrayNode = (ArrayNode) root;
            
            for(int i = 0; i < arrayNode.size(); i++) {
                JsonNode arrayElement = arrayNode.get(i);
                traverse(arrayElement, endPoints, url, routes);
            }
        } else {
            // System.out.println("toString: "+root.toString());
        }
        return null;
    }

    // private List<String> getUrlsForAllPaths(List<String> keys, String url) {
    //     List<String> urls = new ArrayList<String>();
    //     for (String path : keys) {
    //         urls.add(url+path);
    //     }
    //     return urls;
    // }

    private List<String> getNodeKeys(JsonNode node){
        List<String> keys = new ArrayList<>();
        Iterator<String> iterator = node.fieldNames();
        iterator.forEachRemaining(e -> {
            keys.add(e);
        });
        return keys;
    }

    @OPERATION
    public void registerResourceWithEureka(String appName, String uri) throws JsonProcessingException {
        String instanceID = "";
        String hostname = "";
        String port = "";

        System.out.println("URL registerResourceWithEureka Eureka Artifact:"+uri);
        System.out.println("registerResourceWithEureka App Name: "+appName);
        String[] array = registrationStringConcat(uri);
        instanceID = array[0];
        hostname = array[1];
        port = array[2];
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        ObjectNode instance = mapper.createObjectNode();
        ObjectNode portNode = mapper.createObjectNode();
        ObjectNode securePortNode = mapper.createObjectNode();
        ObjectNode dataCenterInfoNode = mapper.createObjectNode();

        instance.put("instanceId", instanceID);
        instance.put("hostName", hostname);
        instance.put("app", appName);
        instance.put("ipAddr", hostname);
        instance.put("status", "UP");
        
        portNode.put("$", port);
        portNode.put("@enabled", "true");

        securePortNode.put("$", "443");
        securePortNode.put("@enabled", "true");
        
        instance.set("port", portNode);
        instance.set("securePort", securePortNode);
        instance.put("statusPageUrl", uri + "/status");
        instance.put("homePageUrl", uri);
        
        dataCenterInfoNode.put("@class", "com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo");
        dataCenterInfoNode.put("name", "MyOwn");
        
        instance.set("dataCenterInfo", dataCenterInfoNode);
        rootNode.set("instance", instance);
        
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
        postRequest("http://localhost:8761/eureka/apps/"+appName, jsonString);

    }

    private String[] registrationStringConcat(String url) {
        String instanceID = "";
        String hostname = "";
        String port = "";

        // String[] split = url.split("//");
        String[] split1 = url.split("/");

        String[] urlAndPort = split1[2].split(":");

        if (urlAndPort[0].equals("127.0.1.1")) {
            instanceID = "localhost:" + urlAndPort[1];
            hostname = "localhost";
        } else {
            instanceID = split1[0];
            hostname = urlAndPort[0];
        }
        port = urlAndPort[1];

        String[] returnArray = { instanceID, hostname, port };
        return returnArray;
    }

    private WebResponse getRequest(String uri) {
        RequestObject requestObject = new RequestObject();
        requestObject.method = "GET";
        requestObject.url = uri;
        requestObject.type = "application/json";
        WebResponse response = WebUtils.sendRequest(requestObject);
        return response;
    }

    private WebResponse postRequest(String uri, String body) {
        RequestObject requestObject = new RequestObject();
        requestObject.method = "POST";
        requestObject.url = uri;
        requestObject.content = body;
        requestObject.type = "application/json";
        try {
            WebResponse response = WebUtils.sendRequest(requestObject);
            // System.out.println(response.getCode());
            // System.out.println(response.getContent());
            return response;
        } catch (Throwable th) {
            th.printStackTrace();
            System.exit(0);
        }
        System.out.println("POST FAILURE");
        return null;
    }

    public static String readFileAsString(String path) throws Exception {
        return new String(Files.readAllBytes(Paths.get(path)));
    }
}