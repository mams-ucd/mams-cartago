package mams.adaptors;

import com.fasterxml.jackson.databind.JsonNode;

public interface InboxAdaptor {
    public abstract boolean receive(JsonNode message);
}