package mams.adaptors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.CartagoException;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import mams.artifacts.Inbox;
import mams.utils.CartagoBackend;

public class DefaultInboxAdaptor implements InboxAdaptor {
    private static ObjectMapper mapper = new ObjectMapper();
    protected Inbox artifact;

    public DefaultInboxAdaptor(Inbox artifact) {
        this.artifact = artifact;
    }

    @Override
    public boolean receive(JsonNode node) {
        try {
            CartagoBackend.getInstance().doAction(artifact.getId(), new Op("receive", node.get("performative").asText(),
                    node.get("sender").asText(), mapper.writeValueAsString(node.get("content"))));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        } catch (ActionFailedException e) {
            e.printStackTrace();
            return false;
        } catch (CartagoException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
}
