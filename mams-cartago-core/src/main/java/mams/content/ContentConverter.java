package mams.content;

import java.util.Map;
import java.util.TreeMap;

public abstract class ContentConverter {
    private static Map<String, ContentConverter> converters = new TreeMap<String, ContentConverter>();
    private static final String ANY_CONTENT_TYPE = "*/*";
    private static  String defaultContentType = "application/json";

    public static void setDefaultContentType(String type) {
        defaultContentType = type;
    }

    /**
     * Return a content converter based on the given type or
     * null if no such type exists.
     * 
     * @param type
     * @return
     */
    public static ContentConverter getConverter(String type) {
        if (type.equals(ANY_CONTENT_TYPE))
            return converters.get(defaultContentType);
        return converters.get(type);
    }

    /**
     * Attempt to resolve the list of types to a specific content converter based on
     * the provided ordering.
     * 
     * @param types
     * @return
     */
    public static ContentConverter getConverter(String[] types) {
        for (String type : types) {
            ContentConverter converter = getConverter(type);
            if (converter != null) return converter;
        }
        return null;
    }

    public static void registerConverter(ContentConverter converter) {
        converters.put(converter.type(), converter);
    }
    
    public abstract String type();
    public abstract String encode(Class<?> type, Object source);
    public abstract <T> T decode(Class<T> type, String content);
}