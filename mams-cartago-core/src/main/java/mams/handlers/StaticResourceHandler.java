package mams.handlers;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.web.Handler;
import mams.web.Path;
import mams.web.WebServer;
import mams.artifacts.StaticResourceArtifact;

public class StaticResourceHandler implements Handler {
    private Map<String, Handler> routes = new TreeMap<String, Handler>();
    private StaticResourceArtifact artifact;
    public Path path;
    
    
    public StaticResourceHandler(StaticResourceArtifact a){
        System.out.println("Creating staticResourceHandler");
        this.artifact = a;
    }
    
    public void addRoute(String id, Handler handler){
        System.out.println(("Adding route " + id));
        routes.put(id, handler);
    }
    
    public void deleteRoute(String id){
        routes.remove(id);
    }

    protected StaticResourceArtifact getArtifact() {
        return artifact;
    }

    public StaticResourceArtifact[] getChildren() {
        StaticResourceArtifact[] array = new StaticResourceArtifact[routes.size()];
        int index = 0;
        for (Handler handler : routes.values()) {
            array[index++] = ((StaticResourceHandler) handler).getArtifact();
        }
        return array;
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {
        this.path = path;
        if (this.path.length() > 0) {
            Handler handler = routes.get(this.path.prefix());
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, this.path);
            }
        } else {
            // System.out.println("Method: " + request.method().toString());
            // System.out.println("Uri: " + request.uri().toString());
            artifact.handle(ctx, request);
        }
	}

    public Set<String> getLinks(){
        return routes.keySet();
    }

}

