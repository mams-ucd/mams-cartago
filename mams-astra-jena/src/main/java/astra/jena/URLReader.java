package astra.jena;

import astra.core.Module;

public class URLReader extends Module {
    @TERM
    public String lastSegment(String url) {
        return url.substring(url.lastIndexOf("/")+1);
    }
}
