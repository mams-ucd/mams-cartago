package astra.jena.jena.reasoner;

import java.util.Map;

import astra.core.Agent;
import astra.formula.Formula;
import astra.reasoner.FormulaUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class TripleFormulaUnifier implements FormulaUnifier {

    @Override
    public boolean isApplicable(Formula source, Formula target) {
        return (source instanceof TripleFormula) && (target instanceof TripleFormula);
    }

    @Override
    public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
        TripleFormula s = (TripleFormula) source;
        TripleFormula t = (TripleFormula) target;

        return Unifier.unify(
            new Term[] {s.getObject(), s.getPredicate(), s.getSubject()},
            new Term[] {t.getObject(), t.getPredicate(), t.getSubject()},
            bindings, agent
        );
    }
    
}