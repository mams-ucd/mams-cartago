package astra.jena.jena.reasoner;

import astra.formula.Formula;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class TripleFormula implements Formula {
    private Term subject;
    private Term predicate;
    private Term object;

    public TripleFormula(Term subject, Term predicate, Term object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public void reIndex() {
        subject.reIndex();
        predicate.reIndex();
        object.reIndex();
    }

    public Term getSubject() {
        return subject;
    }

    public Term getPredicate() {
        return predicate;
    }

    public Term getObject() {
        return object;
    }

    @Override
    public Object accept(LogicVisitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean matches(Formula formula) {
        if (formula instanceof TripleFormula) {
            TripleFormula triple = (TripleFormula) formula;
            return triple.subject.equals(subject) && triple.predicate.equals(predicate) && triple.object.equals(object);
        }
        return false;
    }
 
    public String toString() {
        return "triple(" + subject + ", " + predicate + ", " + object + ")";
    }
}
