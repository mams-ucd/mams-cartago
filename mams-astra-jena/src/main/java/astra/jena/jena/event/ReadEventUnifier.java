package astra.jena.jena.event;


import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class ReadEventUnifier implements EventUnifier<ReadEvent> {

	@Override
	public Map<Integer, Term> unify(ReadEvent source, ReadEvent target, Agent agent) {
		return Unifier.unify(
			new Term[] {source.url()},
			new Term[] {target.url()},
			new HashMap<Integer, Term>(),
			agent);
	}


}
