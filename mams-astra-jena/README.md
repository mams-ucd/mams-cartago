# mams-astra-jena

This module has been developed to provide support for reasoning with semantic resources, such as Turtle, XML/RDF and Json-ld.

The project is built on [Apache Jena](https://jena.apache.org).

The project was created for the purposes of submitting entries to [ATAC-2021](https://all-agents-challenge.github.io/atac2021/)

## Including the module in your project

All ASTRA code is run/configured through Apache Maven. To use this module simply include the following dependency in you pom.xml file:

```
    <dependency>
        <groupId>mams-ucd</groupId>
        <artifactId>mams-astra-jena</artifactId>
        <version>1.1.0</version>
    </dependency> 
```

Note: you should change the version to represent the latest release

You may also be required to declare the repository where the MAMS modules are currently located::

```
    <repositories>
		<repository>
			<id>mams-ucd</id>
			<url>https://gitlab.com/mams-ucd/mams-mvn-repo/raw/master/</url>
		</repository>
    </repositories>
```

## Writing code

This project consists of a set of Java classes that implement ASTRA modules.  ASTRA modules are the mechanism by which additional functionality can be added to ASTRA.  The main modules provided are:

- [KnowledgeStore.java](https://gitlab.com/mams-ucd/mams-cartago/-/blob/master/mams-astra-jena/src/main/java/astra/jena/KnowledgeStore.java)
  - This module provides the basic integration between ASTRA and Jena.  It creates a Jena Model and exposes it to the ASTRA Reasoning Engine, allowing knowledge held in the model to be queried through ASTRA formulae. In essence, this is achieved through the creation of the [TripleFormula](https://gitlab.com/mams-ucd/mams-cartago/-/blob/master/mams-astra-jena/src/main/java/astra/jena/jena/reasoner/TripleFormula.java) class, which implements a custom formula for ASTRA.
  - The main action provided through this module is the `getKnowledge(<url>)` method, which submits a GET request to the specified URL, loads any returned knowledge into the model, and generates a custom event when the model has been updated successfully. The action fails if the request is invalid.
- [RDFSchema.java](https://gitlab.com/mams-ucd/mams-cartago/-/blob/master/mams-astra-jena/src/main/java/astra/jena/RDFSchema.java)
  - This module is designed to help simplify the use of RDF Schema when querying the Jena Model.  Multiple instances of this module can be created (one for each schema prefix declared).
  - The schema is associated with the module instance on declaration (e.g. `module RDFSchema("http://www.w3.org/1999/02/22-rdf-syntax-ns#") rdf;` declared a module for manipulating content from the RDF Syntax ontology).
  - Once created, RDF predicates can be referenced more easily (e.g. `rdf.type` is matched to `http://www.w3.org/1999/02/22-rdf-syntax-ns#type`)
  - The module uses the `auto_formula` feature of ASTRA modules to allow developers to write triples in the format: `<predicate>(<subject>, <object>)` which is more consistent with ASTRA syntax. For example, the triple specifying that some subject is of type `brick:Luminance_Command` can be specified in ASTRA as: `rdf.type(string url1, brick.qualifiedName("Luminance_Command"))` where `brick` refers to a second instance of the RDFSchema module: `module RDFSchema("http://buildsys.org/ontologies/Brick#") brick;`

- [TripleWriter](https://gitlab.com/mams-ucd/mams-cartago/-/blob/master/mams-astra-jena/src/main/java/astra/jena/TurtleWriter.java)
  - This module provides a mechanism for specifying ASTRA formulae that can be converted into Turtle triples.
- [XMLLiteralParser](https://gitlab.com/mams-ucd/mams-cartago/-/blob/master/mams-astra-jena/src/main/java/astra/jena/XMLLiteralParser.java)
  - Provides a set of methods for converting XML literals into ASTRA literals
