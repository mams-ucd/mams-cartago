package mams.javatypes.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.handlers.ResourceHandler;
import mams.javatypes.Constants;
import mams.javatypes.artifacts.TypedResourceArtifact;
import mams.web.Handler;
import mams.web.Path;
import mams.web.WebServer;

public class JavaTypesHandler extends ResourceHandler {
    public JavaTypesHandler(TypedResourceArtifact artifact) {
        super(artifact);
    }

	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        System.out.println("Path: "  +path);
        this.path = path;
        if (this.path.length() > 0) {
            String prefix = this.path.prefix();
            if (prefix.equals(Constants.PROFILE)) {
                System.out.println("SPECIAL CASE");

                // This would be a hook to display the json schema for the resource
            }
            Handler handler = routes.get(prefix);
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, this.path);
            }
        } else if (path.prefix().equals("profile")) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.BAD_GATEWAY);
        } else {
            // System.out.println("Method: " + request.method().toString());
            // System.out.println("Uri: " + request.uri().toString());
            artifact.handle(ctx, request);
        }
	}
}

