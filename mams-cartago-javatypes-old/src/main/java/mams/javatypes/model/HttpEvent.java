package mams.javatypes.model;

import java.util.function.Function;

import mams.utils.HttpContext;
import mams.web.ResponseEntity;

public class HttpEvent {
    public HttpContext context;
    public Function<HttpEvent, ResponseEntity> method;
    public byte[] requestBody;

    public HttpEvent(HttpContext context, Function<HttpEvent, ResponseEntity> method, byte[] bytes) {
        this.context = context;
        this.method = method;
        this.requestBody = bytes;
    }
}
