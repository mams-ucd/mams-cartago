package mams.javatypes.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import mams.artifacts.BaseArtifact;
import mams.handlers.BaseHandler;
import mams.javatypes.handlers.BaseJavaTypesHandler;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class BaseJavaTypesArtifact extends BaseArtifact {
	protected BaseHandler createBaseHandler(String context) {
		return new BaseJavaTypesHandler(this, context);
	}


}
