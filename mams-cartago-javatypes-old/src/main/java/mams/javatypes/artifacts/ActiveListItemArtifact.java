package mams.javatypes.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.web.ResponseEntity;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class ActiveListItemArtifact extends ActiveItemArtifact {
    {
        set(HttpMethod.DELETE, event -> {
            try {
                this.destroyArtifact();
                this.dispose(getId());
                // CartagoBackend.getInstance().doAction(getId(), new Op("destroyArtifact"));
                // CartagoBackend.getInstance().doAction(new Op("disposeArtifact", getId()));

                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getObject()));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        // System.out.println("ADDED DELETE");
    }
}